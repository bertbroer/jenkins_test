#!/bin/bash

# clean output dir
clear
rm -rf ./coverage >/dev/null 2>&1
mkdir coverage >/dev/null 2>&1

make cleanall
# Run coverage hiding stub gcno arcs warnings
make CPPUTEST_USE_GCOV=Y gcov | grep -v 'gcno'
if [ $? -eq 0 ]; then
    gcovr -r . --object-directory=gcov --exclude-directories=output/unittests --html --html-detail --gcov-exclude=supports -o coverage/coverage.html
    echo
    echo "Coverage results generated in coverage/coverage.html"
else
    echo
    echo "Errors detected - no coverage results generated"
fi
