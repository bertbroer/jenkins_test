################################################################################
#           ____                _     _ __  __           _
#          / ___|___  _ __ ___ | |__ (_)  \/  | __ _ ___| |_ ___ _ __
#         | |   / _ \| '_ ` _ \| '_ \| | |\/| |/ _` / __| __/ _ \ '__|
#         | |__| (_) | | | | | | |_) | | |  | | (_| \__ \ ||  __/ |
#          \____\___/|_| |_| |_|_.__/|_|_|  |_|\__,_|___/\__\___|_|
#
#################################################################################
#
#	This make file should be invoked with one of the following targets:
#	- all           => builds an incremental release and debug build
#	- clean         => effectively calls clean-debug and clean-release
#	- clean-debug   => effectively removes the complete debug directory
#	- clean-release => effectively removes the complete release directory
#	- debug         => builds an incremental debug build
#	- help          => display a list of targets (default target)
#	- release       => builds an incremental release build
#
#   The version numbers are automatically updated during the build. You can
#   specify the version manually by adding it to the build command. For instance,
#   to create a release build with version 01.04.3, you can use the command:
#   - make release VERSION=01.04.3
#
#	Upon building target release or debug this make file calls itself again
#	with a destination directory (BUILD_DIR), which will hold all related
#	artifacts, and the following targets:
#	- pre-build         => creates a release or debug sub-directory structure
#	- main-build        => Compiles/assembles the *.c and *.S files and places
#	                       the *.d and *.o files in the release or debug
#	                       directory structure. It finishes with linking all
#	                       the *.o files into an *.elf file while creating
#	                       the *.hex and *.map file as well
#
################################################################################
#
#	Notes:
#	- Any directory name or file name within this project should NOT contain
#	  spaces!
#	- This make file relies on an environment variable (GCC_COMPILER_X_Y_Z)
#	  that tells where the compiler is located on your windows/linux system
#	  (for Jenkins VM machine see the wiki pages)...
#	- When a make file is altered make sure to call target clean first before
#	  calling target release or debug...
#	- You may speed up the make process by including command line option -jx
#	  where x is a number which stands for the amount of cores of your CPU
#	  to use...
#	- This make file does not run when using the Windows shell. Use GIT Bash
#	  instead.
#
################################################################################
#
#	To "make" within the Eclipse IDE:
#	- Set the following "Build Variables":
#	  * Name=BASH_OPTION, Type=String, Value=-c
#	  * Name=BASH_PATH, Type=String, Value="C:/Program Files/Git/bin/bash.exe"
#	- Within the properties of your project (for both configuration debug
#	  and release, bullets below relate to the release configuration):
#	  * Set the C/C++ build, tab page: Builder Settings, item: Build command
#	    to: ${BASH_PATH} ${BASH_OPTION}
#	  * Uncheck checkbox: "Generate Makefiles Automatically"
#	    within: C/C++ build, tab page: Builder Settings
#	  * Set the C/C++ build, tab page: Builder Settings, item: Build directory
#	    to the location of this makefile: (i.e.: ${workspace_loc/<project_name>}
#	  * Set the C/C++ build, tab page: Behavior, item: Build (tick checkbox)
#	    to: "make -j8 release"
#	  * Set the C/C++ build, tab page: Behavior, item: Clean (tick checkbox)
#	    to: "make clean-release"
#
################################################################################

################################################################################
#	Project related names/constants/symbols/flags/options...
#
#	Note:
#	- Variables RELEASE_OUTPUT and DEBUG_OUTPUT can have one of the
#	  following options (used together with arm-none-eabi-objcopy):
#	  * ihex    => Intel hex formatted
#	  * binary  => Binary output
#	  * srec    => s-record formatted
#	  * ...     => other output formats??
#	- Avoid spaces in directory and file names at all costs
################################################################################
PROJECT_NAME = test_project
LINKER_FILE = linker.ld
RELEASE_DIR = release
RELEASE_OUTPUT = ihex
DEBUG_DIR = debug
DEBUG_OUTPUT = ihex


################################################################################
#	Project related include directories...
################################################################################
INCLUDE_DIRS += 


################################################################################
#	Project related source files stored in the following set of source
#	directories...
################################################################################
SOURCE_DIRS += .


################################################################################
#	Project related source files including their relative path...
################################################################################
SOURCE_FILES += 

################################################################################
#	Project related compiler/assembler/linker symbols/flags/options, defined
#	for both release and debug targets...
################################################################################
COMMON_FLAGS += -mcpu=cortex-m4
COMMON_FLAGS += -mthumb
COMMON_FLAGS += -mlittle-endian
COMMON_FLAGS += -mfloat-abi=hard
COMMON_FLAGS += -mfpu=fpv4-sp-d16
COMMON_FLAGS += -fsigned-char
COMMON_FLAGS += -fmessage-length=0
COMMON_FLAGS += -ffunction-sections
COMMON_FLAGS += -fdata-sections
COMMON_FLAGS += -Wall
COMMON_FLAGS += -Wextra
COMMON_FLAGS += -Wno-unused-parameter

COMMON_PREPROCESSOR_FLAGS += -DSTM32L476xx
COMMON_PREPROCESSOR_FLAGS += -DUSE_HAL_DRIVER
COMMON_PREPROCESSOR_FLAGS += -UDEBUGWITHOUTBOOTLOADER
COMMON_PREPROCESSOR_FLAGS += -DIS_MASTERBUS
COMMON_PREPROCESSOR_FLAGS += -DCZ_USE_FLASH_CONFIG_STORAGE
COMMON_PREPROCESSOR_FLAGS += -DDC_METERING_PRE_CALIBRATED_MEASUREMENTS
COMMON_PREPROCESSOR_FLAGS += -DAC_METERING_PRE_CALIBRATED_MEASUREMENTS

COMMON_ASSEMBLER_FLAGS += -x assembler-with-cpp
COMMON_ASSEMBLER_FLAGS += -MMD
COMMON_ASSEMBLER_FLAGS += -MP
COMMON_ASSEMBLER_FLAGS += -MF"$(@:%.o=%.d)"
COMMON_ASSEMBLER_FLAGS += -MT"$(@)"
COMMON_ASSEMBLER_FLAGS += -c
COMMON_ASSEMBLER_FLAGS += -o "$@"

COMMON_COMPILER_FLAGS += -std=gnu11
COMMON_COMPILER_FLAGS += -MMD
COMMON_COMPILER_FLAGS += -MP
COMMON_COMPILER_FLAGS += -MF"$(@:%.o=%.d)"
COMMON_COMPILER_FLAGS += -MT"$(@)"
COMMON_COMPILER_FLAGS += -c
COMMON_COMPILER_FLAGS += -o "$@"

COMMON_LINKER_FLAGS += -T $(LINKER_FILE)
COMMON_LINKER_FLAGS += -Xlinker --gc-sections
COMMON_LINKER_FLAGS += -Wl,-Map,$(BUILD_DIR)/$(PROJECT_NAME).map
COMMON_LINKER_FLAGS += --specs=nano.specs
COMMON_LINKER_FLAGS += -o $(BUILD_DIR)/$(PROJECT_NAME).elf

ifeq ($(BUILD_DIR),$(DEBUG_DIR))
    ASSEMBLERFLAGS = $(COMMON_FLAGS) -O0 -g3 $(COMMON_PREPROCESSOR_FLAGS) -DDEBUG $(INCLUDES) $(COMMON_ASSEMBLER_FLAGS) "$<"
    COMPILERFLAGS = $(COMMON_FLAGS) -O0 -g3 $(COMMON_PREPROCESSOR_FLAGS) -DDEBUG $(INCLUDES) $(COMMON_COMPILER_FLAGS) "$<"
    LINKERFLAGS = $(COMMON_FLAGS) -O0 -g3 $(COMMON_LINKER_FLAGS) $(OBJECT_FILES)
    OUTPUT_FORMAT = $(DEBUG_OUTPUT)
else
    ASSEMBLERFLAGS = $(COMMON_FLAGS) -Os -g $(COMMON_PREPROCESSOR_FLAGS) $(INCLUDES) $(COMMON_ASSEMBLER_FLAGS) "$<"
    COMPILERFLAGS = $(COMMON_FLAGS) -Os -g $(COMMON_PREPROCESSOR_FLAGS) $(INCLUDES) $(COMMON_COMPILER_FLAGS) "$<"
    LINKERFLAGS = $(COMMON_FLAGS) -Os -g $(COMMON_LINKER_FLAGS) $(OBJECT_FILES)
    OUTPUT_FORMAT = $(RELEASE_OUTPUT)
endif


################################################################################
#	Environment Variable holding the path to the compiler of your choosing.
#	The environment variable needs to exist on both your local Windows machine
#	and the (Jenkins) build server.
#
#	Known gcc compilers (on May 6th, 2019):
#	- GCC_COMPILER_4_9_3    => Version V4.9.3 (Windows + Jenkins build server)
#	- GCC_COMPILER_5_4_1    => Version V5.4.1 (Windows only)
#	- GCC_COMPILER_7_2_1    => Version V7.2.1 (Windows + Jenkins build server)
################################################################################
COMPILER_LOCATION = $(GCC_COMPILER_7_2_1)


################################################################################
#	All generic stuff regarding building targets are moved to a generic
#	makefile part (makefile_generic.mk). Usually you do not need to alter this
#	generic makefile...
################################################################################
include makefile_generic.mk

