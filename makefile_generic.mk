################################################################################
#                       ____                      _
#                      / ___| ___ _ __   ___ _ __(_) ___
#                     | |  _ / _ \ '_ \ / _ \ '__| |/ __|
#                     | |_| |  __/ | | |  __/ |  | | (__
#                      \____|\___|_| |_|\___|_|  |_|\___|
#                                    _         __   _
#                    _ __ ___   __ _| | _____ / _(_) | ___
#                   | '_ ` _ \ / _` | |/ / _ \ |_| | |/ _ \
#                   | | | | | | (_| |   <  __/  _| | |  __/
#                   |_| |_| |_|\__,_|_|\_\___|_| |_|_|\___|
#
################################################################################
#
#	This make file should be invoked with one of the following targets:
#	- all           => builds an incremental release and debug build
#	- clean         => effectively calls clean-debug and clean-release
#	- clean-debug   => effectively removes the complete debug directory
#	- clean-release => effectively removes the complete release directory
#	- debug         => builds an incremental debug build
#	- help          => display a list of targets (default target)
#	- release       => builds an incremental release build
#
#	Upon building target release or debug this make file calls itself again
#	with a destination directory (BUILD_DIR), which will hold all related
#	artifacts, and the following targets:
#	- pre-build         => creates a release or debug sub-directory structure
#	- main-build        => Compiles/assembles the *.c and *.S files and places
#	                       the *.d and *.o files in the release or debug
#	                       directory structure. It finishes with linking all
#	                       the *.o files into an *.elf file while creating
#	                       the *.hex and *.map file as well
#
################################################################################
#
#	Notes:
#	- Any directory name or file name within this project should NOT contain
#	  spaces!
#	- This make file relies on an environment variable (GCC_COMPILER_X_Y_Z)
#	  that tells where the compiler is located on your windows/linux system
#	  (for Jenkins VM machine see the wiki pages)...
#	- When a make file is altered make sure to call target clean first before
#	  calling target release or debug...
#	- You may speed up the make process by including command line option -jx
#	  where x is a number which stands for the amount of cores of your CPU
#	  to use...
#	- This make file does not run when using the Windows shell. Use GIT Bash
#	  instead.
#
################################################################################
#
#	To "make" within the Eclipse IDE:
#	- Set the following "Build Variables":
#	  * Name=BASH_OPTION, Type=String, Value=-c
#	  * Name=BASH_PATH, Type=String, Value="C:/Program Files/Git/bin/bash.exe"
#	- Within the properties of your project (for both configuration debug
#	  and release, bullets below relate to the release configuration):
#	  * Set the C/C++ build, tab page: Builder Settings, item: Build command
#	    to: ${BASH_PATH} ${BASH_OPTION}
#	  * Uncheck checkbox: "Generate Makefiles Automatically"
#	    within: C/C++ build, tab page: Builder Settings
#	  * Set the C/C++ build, tab page: Builder Settings, item: Build directory
#	    to the location of this makefile: (i.e.: ${workspace_loc/<project_name>}
#	  * Set the C/C++ build, tab page: Behavior, item: Build (tick checkbox)
#	    to: "make -j8 release"
#	  * Set the C/C++ build, tab page: Behavior, item: Clean (tick checkbox)
#	    to: "make clean-release"
#
################################################################################

################################################################################
#	Calling make without target forces to make the help target.
################################################################################
.DEFAULT_GOAL:=help


################################################################################
#	Executables...
################################################################################
ASTYLE = astyle
COMPILER = $(COMPILER_LOCATION)/arm-none-eabi-gcc
ASSEMBLER = $(COMPILER_LOCATION)/arm-none-eabi-gcc
LINKER = $(COMPILER_LOCATION)/arm-none-eabi-gcc
HEX_BUILDER = $(COMPILER_LOCATION)/arm-none-eabi-objcopy
SIZE_CALCULATOR = $(COMPILER_LOCATION)/arm-none-eabi-size


################################################################################
#	File handling operators...
################################################################################
RM := rm -rf
MKDIR_P := mkdir -p


################################################################################
#	Helper functions. Can only be used/called after they are defined...
################################################################################
get_src_from_dir = $(wildcard $1/*.c) $(wildcard $1/*.S)
get_dirs_from_dirspec = $(wildcard $1)
get_src_from_dir_list = $(foreach dir, $1, $(call get_src_from_dir,$(dir)))
__src_to = $(subst .c,$1, $(subst .S,$1,$2))
src_to = $(addprefix $(BUILD_DIR)/,$(call __src_to,$1,$2))
src_to_o = $(call src_to,.o,$1)


################################################################################
#	Create selections of directories and files...
#
#	In order to force identical builds (identical HEX files) on both your local
#	machine and the Jenkins build server, the order of linking object files
#	must be the same on both platforms. As the list of object file are derived
#	from source files, the source files need to be sorted...
################################################################################
SOURCE_FILES += $(sort $(call get_src_from_dir_list,$(SOURCE_DIRS)))
OBJECT_FILES = $(call src_to_o,$(SOURCE_FILES))
DEPENDENCY_FILES = $(patsubst %.o,%.d,$(OBJECT_FILES))
BUILD_DIR_STRUCTURE += $(BUILD_DIR)
BUILD_DIR_STRUCTURE += $(addprefix $(BUILD_DIR)/,$(SOURCE_DIRS) $(dir $(SOURCE_FILES)))
INCLUDES = $(addprefix -I,$(INCLUDE_DIRS))


################################################################################
#	Determine the output format of the target (defaults to Intel hex)...
################################################################################
ifeq ($(OUTPUT_FORMAT), binary)
	OUTPUT_EXTENSION = bin
else
	ifeq ($(OUTPUT_FORMAT), srec)
		OUTPUT_EXTENSION = srec
	else
		OUTPUT_FORMAT = ihex
		OUTPUT_EXTENSION = hex
	endif
endif


################################################################################
#	Primary targets.
#
#	Note:
#	- Do not use $(MAKE) as it uses the default shell on whatever OS this make
#	  file is running, which on Windows is the poorly windows shell. We need to
#	  make via GIT Bash. Using $SHELL) guarantees that it uses the same shell
#	  as the initially used shell...
#	- Actual building of release and/or debug will only occur when the compiler
#	  location is known.
#	- Make sure the "exit 1" command is without hyphen ('-') otherwise the
#	  Jenkins build server will not see the ommision of a compiler as
#	  a failure.
################################################################################
# All Target
all: release debug ## Make both a release and debug build

# Clean All Target
clean: clean-release clean-debug ## Clean both the release and debug build

# Clean Debug Target
clean-debug: ## Clean the debug build
	-$(RM) $(DEBUG_DIR)

# Clean Release Target
clean-release: ## Clean the release build
	-$(RM) $(RELEASE_DIR)

# Debug Target
debug: check-compiler ## Make a debug build
	"$(SHELL)" -c "make --no-print-directory BUILD_DIR=$(DEBUG_DIR) pre-build"
	"$(SHELL)" -c "make --no-print-directory BUILD_DIR=$(DEBUG_DIR) main-build"

# Release Target
release: check-compiler ## Make a release build
	"$(SHELL)" -c "make --no-print-directory BUILD_DIR=$(RELEASE_DIR) pre-build"
	"$(SHELL)" -c "make --no-print-directory BUILD_DIR=$(RELEASE_DIR) main-build"


################################################################################
#	Show the (public) targets that may be build with this makefile.
#
#	Effectively add a text starting with two pounds characters ("##" ) after
#	each target that needs to be included in the help...
################################################################################
# Displays a list of target options
help:  ## Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nusage: make <target>\n\ntargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  %-13s %s\n", $$1, $$2 }' $(MAKEFILE_LIST)


################################################################################
#	Secondary target: check-compiler.
#
#	When environment variable exists continue, otherwise abort make/build...
################################################################################
# Check compiler Target
check-compiler:
ifeq ($(COMPILER_LOCATION),)
	-@echo ' '
	-@echo 'Can not locate the compiler. Building is aborted!!!'
	-@echo ' '
	exit 1
else
	-@echo ' '
	-@echo "Using compiler: "$(COMPILER)
	-@echo ' '
endif


################################################################################
#	Secondary target: pre-build.
#
#	Target pre-build builds the release or debug directory structure and runs
#	the astyle code formatter...
################################################################################
# Pre-Build Target
pre-build:
	-@$(MKDIR_P) $(BUILD_DIR_STRUCTURE)
	-@echo ' '


################################################################################
#	Secondary target: main-build.
#
#	Target main-build compiles/assembles all source files and stores the
#	results in the directory structure made by pre-build. Finally three
#	additional steps are done:
#	- Linking, creating the *.elf.
#	- converting the *.elf file into a hex file.
#	- generate size info that is shown in stdout.
################################################################################
# Main-build Target
main-build: $(BUILD_DIR)/$(PROJECT_NAME).$(OUTPUT_EXTENSION) $(BUILD_DIR)/$(PROJECT_NAME).siz

# Tool invocations
$(BUILD_DIR)/$(PROJECT_NAME).elf: $(OBJECT_FILES) $(USER_OBJS)
	@echo ' '
	@echo 'Building target: $@'
	@echo 'Invoking: Cross ARM C Linker'
	$(LINKER) $(LINKERFLAGS)
	@echo 'Finished building target: $@'
	@echo ' '

$(BUILD_DIR)/$(PROJECT_NAME).$(OUTPUT_EXTENSION): $(BUILD_DIR)/$(PROJECT_NAME).elf
	@echo 'Invoking: Cross ARM GNU Create Flash Image'
	$(HEX_BUILDER) -O $(OUTPUT_FORMAT) $(BUILD_DIR)/$(PROJECT_NAME).elf  $(BUILD_DIR)/$(PROJECT_NAME).$(OUTPUT_EXTENSION)
	@echo 'Finished building: $(BUILD_DIR)/$(PROJECT_NAME).$(OUTPUT_EXTENSION)'
	@echo ' '

$(BUILD_DIR)/$(PROJECT_NAME).siz: $(BUILD_DIR)/$(PROJECT_NAME).elf
	@echo 'Invoking: Cross ARM GNU Print Size'
	$(SIZE_CALCULATOR) --format=berkeley $(BUILD_DIR)/"$(PROJECT_NAME).elf"
	@echo 'Finished building: $@'
	@echo ' '

$(BUILD_DIR)/%.o: %.c
	@echo 'Cross ARM C Compiler: $<'
	@$(COMPILER) $(COMPILERFLAGS)

$(BUILD_DIR)/%.o: %.S
	@echo 'Cross ARM GNU Assembler: $<'
	@$(ASSEMBLER) $(ASSEMBLERFLAGS)


# Include the dependency files, so that source files will be rebuild when header files are changed.
-include $(DEPENDENCY_FILES) 

.PHONY: all clean main-build pre-build check-compiler release clean-release debug clean-debug help


